package com.chan.recyclerview4.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.chan.recyclerview4.R
import com.chan.recyclerview4.model.UserModel
import kotlinx.android.synthetic.main.fragment_user_profile.*

class UserProfileFragment :Fragment() {
    private lateinit var dataModel: UserModel

    companion object {
        fun newInstant(model: UserModel): UserProfileFragment {
            val fragment = UserProfileFragment()
            val bundle = Bundle()
            bundle.putParcelable("model", model)
            fragment.arguments = bundle
            return fragment


        }

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(savedInstanceState ==null){
            arguments?.let{
                dataModel = it.getParcelable("model")!!
            }
        }
        else{
            dataModel = savedInstanceState.getParcelable("model")!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUserDetail()
    }
    fun setUserDetail(){
        var street = dataModel.address!!.street.toString()
        var suit = dataModel.address!!.suit.toString()
        var city = dataModel.address!!.city.toString()
        var zipcode = dataModel.address!!.zipcode.toString()

        textFirstName.text = dataModel.name!!.toString()
        textLastName.text = dataModel.email!!.toString()
        textBirthday.text = dataModel.phone!!.toString()
        textGender.text = dataModel.website!!.toString()
        textEmail.text = dataModel.company!!.name.toString()
        textLocation.text = "$street$suit$city$zipcode"

    }
    override fun onSaveInstanceState(bundle: Bundle) {
            super.onSaveInstanceState(bundle)
            bundle.putParcelable("model",dataModel)

    }
}