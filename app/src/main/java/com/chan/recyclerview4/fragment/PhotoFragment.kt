package com.chan.recyclerview4.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chan.recyclerview4.R
import com.chan.recyclerview4.adapter.PhotoAdapterItem
import com.chan.recyclerview4.listener.OnItemClickListener
import com.chan.recyclerview4.listener.OnNavigateToUserDetailListener
import com.chan.recyclerview4.model.UserModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_photo.*
import okhttp3.*
import java.io.IOException

class PhotoFragment: Fragment(), OnItemClickListener {
    lateinit var listener: OnNavigateToUserDetailListener
    private lateinit var dataList: ArrayList<UserModel>
    lateinit var layoutManager: RecyclerView.LayoutManager
    lateinit var adapter : PhotoAdapterItem

    companion object{
        fun newInstant (): PhotoFragment{
            return PhotoFragment()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        when (context) {
            is OnNavigateToUserDetailListener -> listener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(savedInstanceState == null){
           dataList = arrayListOf()
        }
        else{
            dataList = savedInstanceState.getParcelableArrayList("photo")!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_photo, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRefreshLayout()
        setLayoutManager()
        setupRecyclerView()

      }

    fun setRefreshLayout() {
        refreshLayout.setOnRefreshListener {
            dataList.clear()
            adapter.notifyDataSetChanged()
            getData()
        }
    }
    fun setLayoutManager(){
        layoutManager = LinearLayoutManager(activity)
    }
    fun setupRecyclerView(){
        recyclerView_main.layoutManager =layoutManager
        recyclerView_main.setHasFixedSize(true)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (dataList.isEmpty()){
            showLoading()
            getData()
        }
        else{
            setAdapter()
        }
    }

    fun showLoading() {
        refreshLayout.isRefreshing = true
    }

    fun hideLoading() {
        refreshLayout.isRefreshing = false
    }
    private fun getData(){

        val endpoint = "https://jsonplaceholder.typicode.com/users"
        val client = OkHttpClient()
        val request = Request.Builder()
            .url(endpoint)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
            }
            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful){
                    response.body?.let{
                        val data = it.string()
                        mapDataToJson(data)
                        activity?.runOnUiThread {
                            hideLoading()
                            setAdapter()
                        }
                    }
                }

            }

        })
    }

    fun mapDataToJson(data: String){
        val gson = Gson()
        val collectionType = object : TypeToken<List<UserModel>>(){}.type

        dataList = gson.fromJson(data,collectionType) as ArrayList<UserModel>
    }

    private fun setAdapter(){
        adapter = PhotoAdapterItem()
        adapter.listener = this@PhotoFragment
        adapter.dataList = dataList
        recyclerView_main.adapter =adapter
    }
    override fun onClicked(position: Int) {
        val model = dataList[position]
        listener.navigateToUserDetailFragment(model)



    }

    override fun test(position: Int) {
    }

    override fun onCheckChange(position: Int) {
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onSaveInstanceState(bundle: Bundle) {
        super.onSaveInstanceState(bundle)
        bundle.putParcelableArrayList("photo",dataList)

    }
}