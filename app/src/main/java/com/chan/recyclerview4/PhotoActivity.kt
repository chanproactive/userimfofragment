package com.chan.recyclerview4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.chan.recyclerview4.fragment.PhotoFragment
import com.chan.recyclerview4.fragment.UserProfileFragment
import com.chan.recyclerview4.listener.OnNavigateToUserDetailListener
import com.chan.recyclerview4.model.UserModel
class PhotoActivity : AppCompatActivity(), OnNavigateToUserDetailListener  {


    private lateinit var dataList: ArrayList<UserModel>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null){
            replacePhotoFragment()
        }
    }


    private fun replacePhotoFragment(){
        var fragment = PhotoFragment.newInstant()
        var transection =  supportFragmentManager.beginTransaction()
        transection.replace(R.id.container,fragment)
        transection.commit()
    }

    override fun navigateToUserDetailFragment(model: UserModel) {
        super.navigateToUserDetailFragment(model)
        replaceUserProfileFragment(model)

    }
    private fun replaceUserProfileFragment(model: UserModel){
        var fragment = UserProfileFragment.newInstant(model)
        var transection = supportFragmentManager.beginTransaction()
        transection.replace(R.id.container,fragment)
        transection.addToBackStack("null")
        transection.commit()

    }


}
