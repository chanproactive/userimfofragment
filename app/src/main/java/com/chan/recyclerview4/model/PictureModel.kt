//package com.chan.recyclerview4.model
//
//import android.os.Parcel
//import android.os.Parcelable
//import com.google.gson.annotations.SerializedName
//
//data class UserModel(@SerializedName("albumId") var albumId :Int,
//                     @SerializedName("id") var id : Int,
//                     @SerializedName("title") var title :String?,
//                     @SerializedName("thumbnailUrl") var thumbnailUrl: String?,
//                     @SerializedName("url") var url: String?
//                        ) : Parcelable {
//    constructor(parcel: Parcel) : this(
//        parcel.readInt(),
//        parcel.readInt(),
//        parcel.readString(),
//        parcel.readString(),
//        parcel.readString()
//    ) {
//    }
//
//    override fun writeToParcel(parcel: Parcel, flags: Int) {
//        parcel.writeInt(albumId)
//        parcel.writeInt(id)
//        parcel.writeString(title)
//        parcel.writeString(thumbnailUrl)
//        parcel.writeString(url)
//    }
//
//    override fun describeContents(): Int {
//        return 0
//    }
//
//    companion object CREATOR : Parcelable.Creator<UserModel> {
//        override fun createFromParcel(parcel: Parcel): UserModel {
//            return UserModel(parcel)
//        }
//
//        override fun newArray(size: Int): Array<UserModel?> {
//            return arrayOfNulls(size)
//        }
//    }
//}
//
