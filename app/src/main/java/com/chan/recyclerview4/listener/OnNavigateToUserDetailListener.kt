package com.chan.recyclerview4.listener

import com.chan.recyclerview4.model.UserModel

interface OnNavigateToUserDetailListener {
    fun navigateToUserDetailFragment(model :UserModel){

    }
}