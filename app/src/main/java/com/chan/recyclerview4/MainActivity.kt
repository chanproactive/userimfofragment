//package com.chan.recyclerview4
//
//import com.chan.recyclerview4.adapter.AdapterItem
//import androidx.appcompat.app.AppCompatActivity
//import android.os.Bundle
//import android.util.Log
//import android.widget.Toast
//import androidx.recyclerview.widget.GridLayoutManager
//import androidx.recyclerview.widget.LinearLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import kotlinx.android.synthetic.main.activity_main.*
//import com.chan.recyclerview4.listener.OnItemClickListener
//import com.chan.recyclerview4.model.CommentFileModel
//import com.chan.recyclerview4.model.UserModel
//import com.google.gson.Gson
//import com.google.gson.reflect.TypeToken
//import kotlinx.android.synthetic.main.fragment_photo.*
//import okhttp3.*
//import java.io.IOException
//
//class MainActivity : AppCompatActivity(), OnItemClickListener {
//    override fun onCheckChange(position: Int) {
//        dataList[position].isChecked = !dataList[position].isChecked
//
//
//
//    }
//
//    override fun test(position: Int) {
//        Toast.makeText(applicationContext, "position ${position+1} is selected", Toast.LENGTH_SHORT).show()
//
//    }
//
//    override fun onClicked(position: Int) {
//        println("position =$position")
//
//    }
//
//    private lateinit var dataList: ArrayList<CommentFileModel>
//    lateinit var layoutManager: RecyclerView.LayoutManager
//    lateinit var adapter : AdapterItem
//
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
////        testData()
//        getData()
//
//        setLayoutManager()
//        setupRecyclerView()
//
//    }
//
//    fun setLayoutManager(){
//        layoutManager = LinearLayoutManager(this)
//
//    }
//    fun setupRecyclerView(){
//        recyclerView_main.layoutManager =layoutManager
//        recyclerView_main.setHasFixedSize(true)
//    }
//    private fun setAdapter(){
//        adapter = AdapterItem()
//        adapter.listener = this@MainActivity
//        adapter.dataList = dataList
//        recyclerView_main.adapter =adapter
//    }
//
//    private fun getData(){
//        val endpoint = "https://jsonplaceholder.typicode.com/posts/1/comments"
//        val client = OkHttpClient()
//        val request = Request.Builder()
//            .url(endpoint)
//            .build()
////        val response: Response =client.newCall(request).execute()
//
//
//        val call = client.newCall(request).enqueue(object : Callback {
//            override fun onFailure(call: Call, e: IOException) {
//
//        }
//
//            override fun onResponse(call: Call, response: Response) {
//                if(response.isSuccessful){
//                    response.body?.let{
//                        val data = it.string()
//
//                        mapDataToJson(data)
//                       runOnUiThread {
//                           setAdapter()
//                       }
//                    }
//                }
//
//            }
//
//        })
//    }
//    fun mapDataToJson(data: String){
//        val gson = Gson()
//        val collectionType = object : TypeToken<List<CommentFileModel>>(){}.type
//
//        dataList = gson.fromJson(data,collectionType) as ArrayList<CommentFileModel>
//
//      //  Log.d("MainActivity","commentS Dada =${model[1].body}")
//
//
//    }
//
//}
